## _Deprecation Notice_
This Pre-Built has been deprecated as of 12-14-2023 and will be end of life on 12-14-2024. The capabilities of this Pre-Built have been replaced by the [Itential Infoblox - DDI - REST Workflow Project](https://gitlab.com/itentialopensource/pre-built-automations/infoblox-ddi-rest)

# Infoblox DNS Management
An pre-built for the management of DNS records in Infoblox

## Table of Contents

- [Infoblox DNS Management](#infoblox-dns-management)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Features](#features)
  - [Requirements](#requirements)
  - [Known Limitations](#known-limitations)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
  - [Additional Information](#additional-information)

## Overview
The Infoblox DNS Management  pre-built enables IAP users to create, modify, and delete DNS record types in the Infoblox platform.  Advantageously, any number of request types (Create, Modify, or Delete) related to any combination of DNS record types (currently available: A, PTR, CNAME, NS, and Fixed Address) may be submitted in one form and processed by one IAP workflow job, providing a streamlined DNS management experience.

Users may create requests to create, delete, or modify DNS records using the associated form.  The workflow job that launches when the form is submitted leverages child workflows to process requests related to the various DNS record types using an Itential adapter that connects to your Infoblox instance.  Upon completion, the workflow provides a summary, in a final manual task, of the successful requests processed by the Infoblox platform.

## Features
* Allows users to perform CRUD operations on multiple DNS record types in Infoblox at one time using a single form.
* Provides a concise report of all successful DNS management requests.

## Requirements
Pre-requisites to use the Infoblox DNS Management pre-built include:

- Itential Automation Platform
  - ^2021.2
This pre-built requires [adapter-infoblox](https://gitlab.com/itentialopensource/adapters/inventory/adapter-infoblox), which must be configured to connect to a running instance of Infoblox.

## Known Limitations
The current version of this pre-built supports management of the following record types in Infoblox: A, PTR, CNAME, NS, and Fixed Address.  In addition, only the nameserver value of an NS record, but not its associated addresses, may be changed at this time.  Lastly, DNS record requests that are not successful are not included in the final manual task that summarizes the status of the DNS record requests processed by Infoblox.

## How to Install
To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.


After the notification displays to inform you that the pre-built has been installed, you should be able to navigate to Operations Manager and verify that the Infoblox DNS Management Operations Manager item has been installed.


## How to Run
The Infoblox DNS Management pre-built may be run manually using IAP's Operations Manager, which leverages associated workflows and JSON forms. Doing so opens a form in which the user may add data for multiple record types to be created or deleted, as well as data that may be used to modify existing records. Note that the various DNS record types are submitted in distinct sections of the same form, and more requests may be added by pressing the "+" button to the right side of a record type.  The form may be submitted by clicking "Run" at the bottom left of the form.

Furthermore, Create and Delete requests are submitted in one section of the form, while Modify requests are handled separately.

Once submitted, a parent workflow employs child workflows based on the DNS record types and requests types provided by the user.

Finally, the workflow provides a summary of the successful DNS record requests processed by Infoblox by way of a manual task, as shown in the [Overview](#overview) section, above.

## Additional Information
Please use your Itential Customer Success account if you need support when using this pre-built.
