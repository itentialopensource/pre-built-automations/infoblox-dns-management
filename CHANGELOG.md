
## 0.1.9 [07-12-2024]

Add deprecation notice

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!35

2024-07-12 17:48:28 +0000

---

## 0.1.8 [12-21-2023]

* Removes relative links for images

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!31

---

## 0.1.7 [12-21-2023]

* Removes image tag from markdown file

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!27

---

## 0.1.6 [12-14-2023]

* Adds deprecation notice

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!25

---

## 0.1.5 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!23

---

## 0.1.4 [07-25-2022]

* Updated workflows with error handling

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!21

---

## 0.1.3 [01-17-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!20

---

## 0.1.2 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!19

---

## 0.1.2 [03-16-2021]

* Merge branch 'minor/LB-404' into 'master'

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!15

---

## 0.1.1-2020.1.1 [07-02-2020]

* Merge branch 'minor/LB-404' into 'master'

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!15

---

## 0.1.1-2020.1.0 [06-18-2020]

* [Patch/Naming] Merge master into release

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!14

---

## 0.1.1 [06-18-2020]

* [Patch/naming] Add patch so release branch increments

See merge request itentialopensource/pre-built-automations/infoblox-dns-management!13

---

## 0.1.0 [06-17-2020]

* Update manifest, package, and readme to reflect correct naming convention

See merge request itentialopensource/pre-built-automations/Infoblox-DNS-Management!10

---

## 0.0.6 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/dns-management-infoblox!9

---

## 0.0.5 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/dns-management-infoblox!9

---

## 0.0.4 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/dns-management-infoblox!9

---

## 0.0.3 [04-09-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/dns-management-infoblox!6

---

## 0.0.2 [04-07-2020]

* added package lock

See merge request itentialopensource/pre-built-automations/dns-management-infoblox!8

---\n\n\n\n\n
